terraform {
  backend "s3" {
    bucket = "tfstatefile2024"
    key = "global/mystatefileforEKS/terraform.tfstate"
    region = "ap-northeast-1"
    dynamodb_table = "backend_table"
    
  }
}
